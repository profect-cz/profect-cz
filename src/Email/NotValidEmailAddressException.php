<?php

declare(strict_types = 1);

namespace Profect\Email;

use Exception;
use Throwable;
use function sprintf;

class NotValidEmailAddressException extends Exception
{

	public function __construct(mixed $id = null, ?Throwable $throwable = null)
	{
		parent::__construct(sprintf('%s is not valid email address.', $id), 0, $throwable);
	}

}
