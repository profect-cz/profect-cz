<?php

declare(strict_types = 1);

namespace Profect\Email;

use Nette\Utils\Strings;
use Nette\Utils\Validators;

class Email
{

	private string $value;

	public function __construct(string $email)
	{
		if (!Validators::isEmail($email)) {
			throw new NotValidEmailAddressException($email);
		}

		$this->value = $email;
	}

	public function getValue(): string
	{
		return Strings::lower($this->value);
	}

	public function equals(Email $email): bool
	{
		return $this->getValue() === $email->getValue();
	}

	public function __toString(): string
	{
		return $this->getValue();
	}

}
