<?php

declare(strict_types = 1);

namespace Profect\Url;

use Nette\Http\Url;

class UrlHelpers
{

	public function getUrlWithUTMParams(string $link, string $utmSource, string $utmMedium, string $utmCampaign): string
	{
		$url = new Url($link);
		$url->setQueryParameter('utm_source', $utmSource);
		$url->setQueryParameter('utm_medium', $utmMedium);
		$url->setQueryParameter('utm_campaign', $utmCampaign);

		return $url->getAbsoluteUrl();
	}

}
