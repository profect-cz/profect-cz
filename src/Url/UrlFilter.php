<?php

declare(strict_types = 1);

namespace Profect\Url;

use function preg_match;

class UrlFilter
{

	public static function filterValue(string $value): ?string
	{
		$isUrl = preg_match('#(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?#', $value);
		if (!$isUrl) {
			return null;
		}

		return $value;
	}

}
