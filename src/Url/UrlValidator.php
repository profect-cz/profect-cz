<?php

declare(strict_types = 1);

namespace Profect\Url;

use function filter_var;
use const FILTER_VALIDATE_URL;

class UrlValidator
{

	public static function isValid(string $url): bool
	{
		return filter_var($url, FILTER_VALIDATE_URL) !== false;
	}

}
