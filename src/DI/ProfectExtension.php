<?php

declare(strict_types = 1);

namespace Profect\DI;

use Nette\DI\CompilerExtension;
use Nette\Schema\Expect;
use Nette\Schema\Schema;
use Profect\UI\GoogleAnalytics\GoogleAnalyticsControlFactory;
use Profect\UI\Metadata\MetadataControlFactory;
use function array_merge;

class ProfectExtension extends CompilerExtension
{

	public function loadConfiguration(): void
	{
		$this->compiler->loadDefinitionsFromConfig(
			$this->loadFromFile(__DIR__ . '/config.neon')['services'],
		);

		$config = (array) $this->getConfig();
		$containerParameteres = $this->getContainerBuilder()->parameters;

		$builder = $this->getContainerBuilder();
		$builder->addFactoryDefinition(null)
			->setImplement(MetadataControlFactory::class)
			->getResultDefinition()
			->setArguments(array_merge($config['metadata'], [
				'debugMode' => $containerParameteres['debugMode'],
			]));

		$builder->addFactoryDefinition(null)
			->setImplement(GoogleAnalyticsControlFactory::class)
			->getResultDefinition()
			->setArguments($config['googleAnalytics']);
	}

	public function getConfigSchema(): Schema
	{
		return Expect::structure([
			'metadata' => Expect::array([
				'author' => Expect::string(),
				'titlePostfix' => Expect::string(),
			]),
			'googleAnalytics' => Expect::array([
				'id' => Expect::string(),
			]),
		]);
	}

}
