<?php

declare(strict_types = 1);

namespace Profect\Country;

class Country extends \Dogma\Country\Country
{

	/** @return array<string> */
	public static function getLabelDefinitions(): array
	{
		$countries = [];
		foreach (self::getAllowedValues() as $country) {
			$country = self::get($country);
			$countries[$country->getValue()] = $country->getName();
		}

		return $countries;
	}

}
