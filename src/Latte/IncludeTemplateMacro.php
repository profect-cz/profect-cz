<?php

declare(strict_types = 1);

namespace Profect\Latte;

use Latte\Compiler;
use Latte\MacroNode;
use Latte\Macros\MacroSet;
use Latte\PhpWriter;

class IncludeTemplateMacro extends MacroSet
{

	public function macroIncludeTemplate(MacroNode $node, PhpWriter $writer): string
	{
		return $writer->write('/* line ' . $node->startLine . ' */
			$this->createTemplate(ROOT_DIR . "/app/UI" . %node.word, %node.array? + $this->params, "macroIncludeTemplate")->renderToContentType(\Latte\Engine::CONTENT_HTML);', []);
	}

	public static function install(Compiler $compiler): void
	{
		$me = new self($compiler);

		$me->addMacro('includeTemplate', [$me, 'macroIncludeTemplate']);
	}

}
