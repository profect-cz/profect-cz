<?php

declare(strict_types = 1);

namespace Profect\Latte;

use Latte\Compiler;
use Latte\MacroNode;
use Latte\Macros\MacroSet;
use Latte\PhpWriter;

class IfAllowedMacro extends MacroSet
{

	public function ifAllowed(MacroNode $node, PhpWriter $writer): string
	{
		$scope = $node->tokenizer->fetchWord();

		return 'if ($this->global->uiPresenter->getUser()->isAllowed('
			. ($scope ? $writer->formatWord($scope) : 'new App\Model\GlobalScope()') . ')) {';
	}

	public static function install(Compiler $compiler): void
	{
		$macros = new self($compiler);
		$macros->addMacro('ifAllowed', [$macros, 'ifAllowed'], '}');
	}

}
