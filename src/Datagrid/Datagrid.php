<?php

declare(strict_types = 1);

namespace Profect\Datagrid;

use Ublaboo\DataGrid\Column\ColumnDateTime;
use Ublaboo\DataGrid\DataGrid as DG;

class Datagrid extends DG
{

	/**
	 * @phpcsSuppress SlevomatCodingStandard.TypeHints.ParameterTypeHint.MissingNativeTypeHint
	 * @param string $key
	 * @param string $name
	 * @param string|null $column
	 */
	public function addColumnDate($key, $name, $column = null): ColumnDateTime
	{
		$columnDateTime = new ColumnDateTime($this, $key, $column ?: $key, $name);
		$columnDateTime->setFormat('d. m. Y')
			->getElementPrototype('th')
			->setAttribute('width', '100px');

		/** @var ColumnDateTime */
		return parent::addColumn($key, $columnDateTime);
	}

	/**
	 * @phpcsSuppress SlevomatCodingStandard.TypeHints.ParameterTypeHint.MissingNativeTypeHint
	 * @param string $key
	 * @param string $name
	 * @param string|null $column
	 */
	public function addColumnDateTime($key, $name, $column = null): ColumnDateTime
	{
		$columnDateTime = new ColumnDateTime($this, $key, $column ?: $key, $name);
		$columnDateTime->setFormat('d.m.Y \v H:i')
			->getElementPrototype('th')
			->setAttribute('width', '160px');

		/** @var ColumnDateTime */
		return parent::addColumn($key, $columnDateTime);
	}

}
