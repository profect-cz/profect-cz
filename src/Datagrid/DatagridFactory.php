<?php

declare(strict_types = 1);

namespace Profect\Datagrid;

class DatagridFactory
{

	public function create(): Datagrid
	{
		$grid = new Datagrid();
		$grid->setDefaultPerPage(20);
		$grid->setTranslator(DatagridTranslator::create());

		return $grid;
	}

}
