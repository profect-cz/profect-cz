<?php

declare(strict_types = 1);

namespace Profect\Console;

use Profect\Enum\Enum;

enum ConsoleResult: int
{

	use Enum;

	case OK = 0;
	case ERROR = 1;

}
