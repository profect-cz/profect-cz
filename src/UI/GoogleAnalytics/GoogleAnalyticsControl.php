<?php

declare(strict_types = 1);

namespace Profect\UI\GoogleAnalytics;

use Profect\UI\Control\Control;

class GoogleAnalyticsControl extends Control
{

	public function __construct(private ?string $id)
	{
	}

	public function render(): void
	{
		$this->setParameters([
			'id' => $this->id,
		]);

		$this->renderView('GoogleAnalyticsControl.latte');
	}

}
