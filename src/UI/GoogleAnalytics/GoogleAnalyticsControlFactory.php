<?php

declare(strict_types = 1);

namespace Profect\UI\GoogleAnalytics;

interface GoogleAnalyticsControlFactory
{

	public function create(): GoogleAnalyticsControl;

}
