<?php

declare(strict_types = 1);

namespace Profect\UI\Js;

use Nette\Utils\Strings;
use Profect\Assets\AssetsService;
use Profect\UI\Control\Control;
use function sprintf;

class JsControl extends Control
{

	public function __construct(private AssetsService $assetsService)
	{
	}

	public function render(mixed ...$scripts): void
	{
		$this->setParameters([
			'paths' => $this->getPaths($scripts),
		]);

		$this->renderView('JsControl.latte');
	}

	/**
	 * @param array<string> $scripts
	 * @return array<string>
	 */
	private function getPaths(array $scripts): array
	{
		$paths = [];
		foreach ($scripts as $script) {
			if (Strings::startsWith($script, '//') || !Strings::startsWith($script, '/')) {
				$paths[] = $script;
				continue;
			}

			$paths[] = sprintf('%s?v=%s', $script, $this->assetsService->getHash($script));
		}

		return $paths;
	}

}
