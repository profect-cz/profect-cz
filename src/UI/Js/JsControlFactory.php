<?php

declare(strict_types = 1);

namespace Profect\UI\Js;

interface JsControlFactory
{

	public function create(): JsControl;

}
