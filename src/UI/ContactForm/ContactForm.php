<?php

declare(strict_types = 1);

namespace Profect\UI\ContactForm;

use Profect\Form\Form;
use function _;

class ContactForm extends Form
{

	public function __construct()
	{
		parent::__construct();

		$this->addText('name', _('Jméno'))
			->setRequired(_('Zadejte prosím vaše jméno'));

		$this->addEmail('email', _('E-mail'))
			->setRequired(_('Zadejte prosím vaši e-mailovou adresu.'));

		$this->addTextArea('message', _('Zpráva'))
			->setRequired(_('Zadejte prosím text zprávy.'));

		$this->addSubmit('submit', _('Odeslat zprávu'));
	}

}
