<?php

declare(strict_types = 1);

namespace Profect\UI\ContactForm;

interface ContactFormFactory
{

	public function create(): ContactForm;

}
