<?php

declare(strict_types = 1);

namespace Profect\UI\Css;

interface CssControlFactory
{

	public function create(): CssControl;

}
