<?php

declare(strict_types = 1);

namespace Profect\UI\Css;

use Nette\Utils\Strings;
use Profect\Assets\AssetsService;
use Profect\UI\Control\Control;
use function sprintf;

class CssControl extends Control
{

	public function __construct(private AssetsService $assetsService)
	{
	}

	public function render(mixed ...$styles): void
	{
		$this->setParameters([
			'paths' => $this->getPaths($styles),
		]);

		$this->renderView('CssControl.latte');
	}

	public function renderDirect(mixed ...$styles): void
	{
		$this->setParameters([
			'paths' => $this->getPaths($styles),
			'includeCss' => fn (string $style): string => $this->assetsService->getStyleFromPath($style),
		]);

		$this->renderView('CssControlDirect.latte');
	}

	/**
	 * @param array<string> $styles
	 * @return array<string>
	 */
	private function getPaths(array $styles): array
	{
		$paths = [];
		foreach ($styles as $style) {
			$isPlugin = Strings::startsWith($style, '/');
			if ($isPlugin) {
				$paths[] = $style;
				continue;
			}

			$path = sprintf('/dist/css/%s.css', $style);

			$paths[] = sprintf('%s?v=%s', $path, $this->assetsService->getHash($path));
		}

		return $paths;
	}

}
