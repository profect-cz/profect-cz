<?php

declare(strict_types = 1);

namespace Profect\UI\FlashMessage;

use Profect\Enum\Enum;

enum FlashMessageType: string
{

	use Enum;

	case ERROR = 'danger';
	case SUCCESS = 'success';

}
