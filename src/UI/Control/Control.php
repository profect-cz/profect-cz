<?php

declare(strict_types = 1);

namespace Profect\UI\Control;

use Profect\Translator\Translator;
use Profect\UI\FlashMessage\FlashMessageType;
use ReflectionClass;
use function dirname;

/** @method getTemplate() Template */
class Control extends \Nette\Application\UI\Control
{

	public function renderView(string $view): void
	{
		$template = $this->getTemplate();
		$template->setTranslator(new Translator());
		$template->setFile($this->getPath() . '/' . $view);
		$template->render();
	}

	/** @param array<mixed> $parameters */
	public function setParameters(array $parameters): void
	{
		$template = $this->getTemplate();
		foreach ($parameters as $key => $item) {
			$template->$key = $item;
		}
	}

	public function successMessage(string $message): void
	{
		$this->presenter->flashMessage($message, FlashMessageType::SUCCESS->value);
	}

	public function errorMessage(string $message): void
	{
		$this->presenter->flashMessage($message, FlashMessageType::ERROR->value);
	}

	private function getPath(): string
	{
		/** @var string $path */
		$path = $this->getReflectionClass()->getFileName();

		return dirname($path);
	}

	/** @return ReflectionClass<self> */
	private function getReflectionClass(): ReflectionClass
	{
		return new ReflectionClass($this);
	}

}
