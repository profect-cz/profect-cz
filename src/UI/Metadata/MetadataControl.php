<?php

declare(strict_types = 1);

namespace Profect\UI\Metadata;

use Nette\Http\IRequest;
use Profect\Robots\RobotsType;
use Profect\UI\Control\Control;
use function sprintf;

class MetadataControl extends Control
{

	private bool $allowIndex;

	private ?string $description = null;

	private ?string $title = null;

	public function __construct(
		private IRequest $request,
		private string $author,
		private string $titlePostfix,
		bool $debugMode,
	)
	{
		$this->allowIndex = !$debugMode;
	}

	public function disallowIndex(): void
	{
		$this->allowIndex = false;
	}

	public function setTitle(string $title): void
	{
		$this->title = $title;
	}

	public function setDescription(string $description): void
	{
		$this->description = $description;
	}

	public function render(): void
	{
		$this->setParameters([
			'author' => $this->author,
			'description' => $this->description,
			'titlePostfix' => $this->titlePostfix,
			'robots' => $this->getRobots(),
			'title' => $this->title === null ? $this->titlePostfix : sprintf('%s | %s', $this->title, $this->titlePostfix),
			'url' => $this->request->getUrl(),
		]);
		$this->renderView('MetadataControl.latte');
	}

	private function getRobots(): string
	{
		return $this->allowIndex
			? RobotsType::INDEX_FOLLOW->value
			: RobotsType::NOINDEX_NOFOLLOW->value;
	}

}
