<?php

declare(strict_types = 1);

namespace Profect\UI\Metadata;

interface MetadataControlFactory
{

	public function create(): MetadataControl;

}
