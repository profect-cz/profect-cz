<?php

declare(strict_types = 1);

namespace Profect\ZipCode;

class ZipCode
{

	private string $value;

	public function __construct(string $zipCode)
	{
		$this->value = ZipCodeFormatter::format($zipCode);
	}

	public function getValue(): string
	{
		return $this->value;
	}

	public function equals(ZipCode $email): bool
	{
		return $this->getValue() === $email->getValue();
	}

	public function __toString(): string
	{
		return $this->value;
	}

}
