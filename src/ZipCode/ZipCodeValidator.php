<?php

declare(strict_types = 1);

namespace Profect\ZipCode;

class ZipCodeValidator
{

	public static function isValid(string $zipCode): bool
	{
		return ZipCodeFilter::filterValue($zipCode) !== null;
	}

}
