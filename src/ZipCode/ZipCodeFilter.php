<?php

declare(strict_types = 1);

namespace Profect\ZipCode;

use Nette\Utils\Strings;
use function preg_replace;

class ZipCodeFilter
{

	public static function filterValue(string $value): ?string
	{
		$zipCode = preg_replace('~\D~', '', $value);
		if ($zipCode === null) {
			return null;
		}
		if (Strings::length($zipCode) !== 5) {
			return null;
		}

		return $zipCode;
	}

}
