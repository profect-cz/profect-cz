<?php

declare(strict_types = 1);

namespace Profect\ZipCode;

use Nette\Utils\Strings;

class ZipCodeFormatter
{

	private const PATTERN = '/\D/';

	public static function format(string $zipCode): string
	{
		return Strings::replace($zipCode, self::PATTERN);
	}

}
