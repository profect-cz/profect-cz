<?php

declare(strict_types = 1);

namespace Profect\Phone;

use Nette\Utils\Strings;

class PhoneFilter
{

	private const PATTERN = '~[^+0-9]~';

	public static function filterValue(string $value): ?string
	{
		return Strings::replace($value, self::PATTERN);
	}

}
