<?php

declare(strict_types = 1);

namespace Profect\Phone;

use Nette\Utils\Strings;
use function sprintf;

class PhoneFormatter
{

	public static function format(string $zipCode): string
	{
		return sprintf(
			'+420 %s %s %s',
			Strings::substring($zipCode, 0, 3),
			Strings::substring($zipCode, 3, 3),
			Strings::substring($zipCode, 6),
		);
	}

}
