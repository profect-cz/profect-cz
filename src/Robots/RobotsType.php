<?php

declare(strict_types = 1);

namespace Profect\Robots;

use Profect\Enum\Enum;

enum RobotsType: string
{

	use Enum;

	case INDEX_FOLLOW = 'index, follow';
	case NOINDEX_NOFOLLOW = 'noindex, nofollow';

}
