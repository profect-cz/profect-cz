<?php

declare(strict_types = 1);

namespace Profect\Cache;

use Nette\Caching\Cache;
use Nette\Caching\IStorage;

class CacheService
{

	private Cache $cache;

	public function __construct(IStorage $storage)
	{
		$this->cache = new Cache($storage);
	}

	/** @param array<mixed>|null $dependencies */
	public function save(string $key, mixed $data, ?array $dependencies = null): void
	{
		$this->cache->save($key, $data, $dependencies);
	}

	public function load(string $key, ?callable $fallback = null): mixed
	{
		return $this->cache->load($key, $fallback);
	}

	public function remove(string $key): void
	{
		$this->cache->remove($key);
	}

}
