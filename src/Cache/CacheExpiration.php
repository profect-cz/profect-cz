<?php

declare(strict_types = 1);

namespace Profect\Cache;

use Profect\Enum\Enum;

enum CacheExpiration: int
{

	use Enum;

	case MIN_5 = 300;
	case MIN_10 = 600;
	case MIN_30 = 1800;
	case HOUR_1 = 3600;
	case HOUR_2 = 7200;
	case HOUR_3 = 10800;
	case DAY_1 = 86400;

}
