<?php

declare(strict_types = 1);

namespace Profect\Form;

use Nette\Forms\Container as NetteContainer;
use Nette\Forms\Controls\TextArea;
use Nette\Forms\Controls\TextInput;
use Nette\Utils\Validators;
use Profect\Email\Email;
use Profect\Phone\PhoneFilter;
use Profect\Url\UrlFilter;
use Profect\Url\UrlValidator;
use Profect\ZipCode\ZipCodeFilter;
use Stringable;
use Webmozart\Assert\Assert;
use function _;
use function sprintf;

class Container extends NetteContainer
{

	/**
	 * @phpcsSuppress SlevomatCodingStandard.TypeHints.ParameterTypeHint.MissingNativeTypeHint
	 * @param string $name
	 * @param string|null $label
	 * @param int|null $cols
	 * @param int|null $maxLength
	 */
	public function addText($name, $label = null, $cols = null, $maxLength = null): TextInput
	{
		return $this[$name] = self::addTextToForm($label, $maxLength);
	}

	/**
	 * @phpcsSuppress SlevomatCodingStandard.TypeHints.ParameterTypeHint.MissingNativeTypeHint
	 * @param string $name
	 * @param string|null $label
	 */
	public function addEmail($name, $label = null): TextInput
	{
		return $this[$name] = self::addEmailToForm($label);
	}

	/**
	 * @phpcsSuppress SlevomatCodingStandard.TypeHints.ParameterTypeHint.MissingNativeTypeHint
	 * @param string $name
	 * @param string|null $label
	 * @param int|null $cols
	 * @param int|null $size
	 */
	public function addPassword($name, $label = null, $cols = null, $size = null): TextInput
	{
		return $this[$name] = self::addPasswordToForm($label, $size);
	}

	public function addFloat(string $name, string|Stringable|null $label = null): TextInput
	{
		return $this[$name] = self::addFloatToForm($label);
	}

	public function addZipCode(string $name, string $label): TextInput
	{
		return $this[$name] = self::addZipCodeToForm($label);
	}

	public function addPhone(string $name, string $label): TextInput
	{
		return $this[$name] = self::addPhoneToForm($label);
	}

	public function addPhoneLocal(string $name, string $label): TextInput
	{
		return $this[$name] = self::addPhoneLocalToForm($label);
	}

	/**
	 * @phpcsSuppress SlevomatCodingStandard.TypeHints.ParameterTypeHint.MissingNativeTypeHint
	 * @param string $name
	 * @param string|null $label
	 * @param int|null $cols
	 * @param int|null $rows
	 */
	public function addTextArea($name, $label = null, $cols = null, $rows = null): TextArea
	{
		return $this[$name] = (new TextArea($label))
			->setHtmlAttribute('cols', $cols)
			->setHtmlAttribute('rows', $rows);
	}

	public static function addTextToForm(?string $label, ?int $maxLength = null): TextInput
	{
		return (new TextInput($label, $maxLength))
			->setNullable();
	}

	public static function addTextAreaToForm(?string $label = null, ?int $cols = null, ?int $rows = 5): TextArea
	{
		return (new TextArea($label))
			->setAttribute('rows', $rows)
			->setNullable();
	}

	public static function addFloatToForm(string|Stringable|null $label, int $min = 0, ?int $max = null): TextInput
	{
		Assert::false($min > $max);

		$input = new TextInput($label);
		$input
			->setNullable()
			->addFilter(static fn ($value): ?float => $value === null ? null : (float) $value)
			->addCondition(Form::FILLED)
			->addRule(Form::FLOAT)
			->addRule(Form::MIN, _('Nejmenší povolená hodnota je %d.'), $min);

		if ($max !== null) {
			$input->addRule(Form::MAX, _('Nejvyšší povolená hodnota je %d.'), $max);
		}

		return $input;
	}

	public static function addUrlToForm(string $label): TextInput
	{
		return (new TextInput($label))
			->setNullable()
			->addRule(static fn (TextInput $input): bool => UrlValidator::isValid($input->getValue()), sprintf(_('Zadejte prosím platnou URL do pole "%s"'), $label))
			->addFilter(static fn ($value): ?string => UrlFilter::filterValue($value));
	}

	public static function addZipCodeToForm(string $label): TextInput
	{
		$input = new TextInput($label);
		$input->setNullable()
			->addCondition(Form::FILLED)
			->addRule(Form::MAX_LENGTH, _('Zadejte prosím platné PSČ'), 6)
			->addRule(static fn (TextInput $textInput): bool => ZipCodeFilter::filterValue($textInput->getValue()) !== null, _('Zadejte prosím platné PSČ'))
			->addFilter(static fn (string $value): ?string => ZipCodeFilter::filterValue($value));

		return $input;
	}

	public static function addEmailToForm(?string $label = null): TextInput
	{
		$input = new TextInput($label);
		$input->setType('email')
			->setNullable()
			->addCondition(Form::FILLED)
			->addRule(Form::EMAIL, _('Zadejte prosím platnou e-mailovou adresu'))
			->addRule(static fn (TextInput $textInput): bool => Validators::isEmail($textInput->getValue()), _('Zadejte prosím platnou e-mailovou adresu'))
			->addFilter(static fn (string $value): Email => new Email($value));

		return $input;
	}

	public static function addPhoneToForm(string $label): TextInput
	{
		$input = new TextInput($label);
		$input
			->setNullable()
			->addCondition(Form::FILLED)
			->addRule(Form::PATTERN, _('Zadejte prosím platné telefonní číslo.'), '(\+420)? ?[1-9][0-9]{2} ?[0-9]{3} ?[0-9]{3}$')
			->addFilter(static fn (string $value): ?string => PhoneFilter::filterValue($value));

		return $input;
	}

	public static function addPhoneLocalToForm(string $label): TextInput
	{
		$input = new TextInput($label);
		$input
			->setNullable()
			->addCondition(Form::FILLED)
			->addRule(Form::PATTERN, _('Zadejte prosím platné telefonní číslo.'), '[1-9][0-9]{2} ?[0-9]{3} ?[0-9]{3}$')
			->addFilter(static fn (string $value): ?string => PhoneFilter::filterValue($value));

		return $input;
	}

	public static function addPasswordToForm(?string $label = null, ?int $size = 6): TextInput
	{
		$input = new TextInput($label, $size);
		$input->setHtmlAttribute('size', $size)
			->setNullable()
			->setHtmlType('password');

		return $input;
	}

}
