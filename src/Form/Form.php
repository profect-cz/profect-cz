<?php

declare(strict_types = 1);

namespace Profect\Form;

use Nette\Forms\Controls\TextArea;
use Nette\Forms\Controls\TextInput;
use Stringable;

class Form extends \Nette\Application\UI\Form
{

	private bool $tinymceInitialized = false;

	/**
	 * @phpcsSuppress SlevomatCodingStandard.TypeHints.NullableTypeForNullDefaultValue.NullabilitySymbolRequired
	 * @phpcsSuppress SlevomatCodingStandard.TypeHints.ParameterTypeHint.MissingNativeTypeHint
	 * @param string|null $label
	 */
	public function addText(string $name, $label = null, ?int $cols = null, ?int $maxLength = null): TextInput
	{
		return $this[$name] = Container::addTextToForm($label, $maxLength);
	}

	public function addTinymce(string $name, string $label, string $tinymceClassType, ?int $cols = null, ?int $rows = 5): TextArea
	{
		if (!$this->tinymceInitialized) {
			$this->getElementPrototype()
				->addAttributes(['onsubmit' => 'tinyMCE.triggerSave()']);
			$this->tinymceInitialized = true;
		}

		$textArea = $this->addTextArea($name, $label, $cols, $rows);
		$textArea->setHtmlAttribute('class', $tinymceClassType);

		return $textArea;
	}

	public function addUrl(string $name, string $label): TextInput
	{
		return $this[$name] = Container::addUrlToForm($label);
	}

	public function addZipCode(string $name, string $label): TextInput
	{
		return $this[$name] = Container::addZipCodeToForm($label);
	}

	public function addFloat(string $name, string|Stringable|null $label = null): TextInput
	{
		return $this[$name] = Container::addFloatToForm($label);
	}

	/**
	 * @phpcsSuppress SlevomatCodingStandard.TypeHints.ParameterTypeHint.MissingNativeTypeHint
	 * @param string $name
	 * @param string|null $label
	 * @param int|null $cols
	 * @param int|null $rows
	 */
	public function addTextArea($name, $label = null, $cols = null, $rows = 5): TextArea
	{
		return $this[$name] = Container::addTextAreaToForm($label, $cols, $rows);
	}

	/**
	 * @phpcsSuppress SlevomatCodingStandard.TypeHints.ParameterTypeHint.MissingNativeTypeHint
	 * @param string $name
	 * @param string|null $label
	 */
	public function addEmail($name, $label = null): TextInput
	{
		return $this[$name] = Container::addEmailToForm($label);
	}

	/**
	 * @phpcsSuppress SlevomatCodingStandard.TypeHints.ParameterTypeHint.MissingNativeTypeHint
	 * @param string $name
	 * @param string|null $label
	 * @param int|null $cols
	 * @param int|null $maxLength
	 */
	public function addPassword($name, $label = null, $cols = null, $maxLength = null): TextInput
	{
		return $this[$name] = Container::addPasswordToForm($label, $maxLength);
	}

	public function addPhone(string $name, string $label): TextInput
	{
		return $this[$name] = Container::addPhoneToForm($label);
	}

	public function addPhoneLocal(string $name, string $label): TextInput
	{
		return $this[$name] = Container::addPhoneLocalToForm($label);
	}

}
