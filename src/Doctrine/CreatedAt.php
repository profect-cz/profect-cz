<?php

declare(strict_types = 1);

namespace Profect\Doctrine;

use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;

trait CreatedAt
{

	/** @ORM\Column(type="datetime_immutable") */
	protected DateTimeImmutable $createdAt;

	public function getCreatedAt(): DateTimeImmutable
	{
		return $this->createdAt;
	}

	private function markAsCreated(): void
	{
		$this->createdAt = new DateTimeImmutable();
	}

}
