<?php

declare(strict_types = 1);

namespace Profect\Doctrine;

use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;

trait Timestampable
{

	/** @ORM\Column(type="datetime_immutable") */
	protected DateTimeImmutable $createdAt;

	/** @ORM\Column(type="datetime_immutable") */
	protected DateTimeImmutable $updatedAt;

	public function getCreatedAt(): DateTimeImmutable
	{
		return $this->createdAt;
	}

	public function getUpdatedAt(): DateTimeImmutable
	{
		return $this->updatedAt;
	}

	private function markAsCreated(): void
	{
		$this->createdAt = new DateTimeImmutable();
		$this->markAsModified();
	}

	private function markAsModified(): void
	{
		$this->updatedAt = new DateTimeImmutable();
	}

}
