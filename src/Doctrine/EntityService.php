<?php

declare(strict_types = 1);

namespace Profect\Doctrine;

use DateTime;
use DateTimeImmutable;
use ReflectionClass;
use Webmozart\Assert\Assert;
use function in_array;
use function is_object;
use function method_exists;
use const DATE_ATOM;

class EntityService
{

	private const ENTITY_DIFF_IGNORE_COLUMNS = [
		'createdAt',
		'updatedAt',
	];

	/** @return array<mixed> */
	public function getEntityDiffColumns(object $original, object $new): array
	{
		$originalEntity = clone $original;
		$newEntity = clone $new;

		$reflectionOld = new ReflectionClass($originalEntity);
		$reflectionNew = new ReflectionClass($newEntity);

		Assert::eq($reflectionOld->getName(), $reflectionNew->getName(), 'Cannot compare different entities');

		$diff = [];
		foreach ($reflectionOld->getProperties() as $originalProperty) {
			if (in_array($originalProperty->getName(), self::ENTITY_DIFF_IGNORE_COLUMNS, true)) {
				continue;
			}

			$newProperty = $reflectionNew->getProperty($originalProperty->getName());

			$originalProperty->setAccessible(true);
			$newProperty->setAccessible(true);

			$newValue = $newProperty->getValue($newEntity);
			$oldValue = $originalProperty->getValue($originalEntity);

			if ($oldValue instanceof DateTime || $oldValue instanceof DateTimeImmutable) {
				$oldValue = $oldValue->format(DATE_ATOM);
			}
			if ($newValue instanceof DateTime || $newValue instanceof DateTimeImmutable) {
				$newValue = $newValue->format(DATE_ATOM);
			}

			if ($newValue === $oldValue) {
				continue;
			}

			if (is_object($newValue) && method_exists($newValue, 'getId')) {
				$oldValue = $oldValue?->getId();
				$newValue = $newValue->getId();
			}

			$diff[$originalProperty->getName()] = [
				'before' => $oldValue,
				'after' => $newValue,
			];
		}

		return $diff;
	}

}
