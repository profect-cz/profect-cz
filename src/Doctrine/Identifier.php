<?php

declare(strict_types = 1);

namespace Profect\Doctrine;

use Doctrine\ORM\Mapping as ORM;
use Webmozart\Assert\Assert;

trait Identifier
{

	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer", nullable=true)
	 * @ORM\GeneratedValue(strategy="IDENTITY")
	 */
	private ?int $id = null;

	final public function getId(): int
	{
		Assert::notNull($this->id, 'Entity havent been persisted yet.');

		return $this->id;
	}

	public function __clone()
	{
		$this->id = null;
	}

}
