<?php

declare(strict_types = 1);

namespace Profect\Doctrine;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

trait Treeable
{

	/**
	 * @var Collection<int, self>
	 *
	 * @ORM\OneToMany(targetEntity=self::class, mappedBy="parent")
	 * @ORM\OrderBy({"lft" = "ASC"})
	 */
	private Collection $children;

	/**
	 * @Gedmo\TreeLeft
	 * @ORM\Column(name="tree_lft", type="integer")
	 */
	private ?int $lft = null;

	/**
	 * @Gedmo\TreeLevel
	 * @ORM\Column(name="tree_lvl", type="integer")
	 */
	private ?int $lvl = null;

	/**
	 * @Gedmo\TreeParent
	 * @ORM\ManyToOne(targetEntity=self::class, inversedBy="children")
	 * @ORM\JoinColumn(name="tree_parent", referencedColumnName="id", onDelete="CASCADE")
	 */
	private ?self $parent = null;

	/**
	 * @Gedmo\TreeRight
	 * @ORM\Column(name="tree_rgt", type="integer")
	 */
	private ?int $rgt = null;

	/**
	 * @Gedmo\TreeRoot
	 * @ORM\ManyToOne(targetEntity=self::class)
	 * @ORM\JoinColumn(name="tree_root", referencedColumnName="id", onDelete="CASCADE")
	 */
	private ?self $root = null;

	/** @return Collection<int, self> */
	public function getChildren(): Collection
	{
		return $this->children;
	}

	public function getLft(): ?int
	{
		return $this->lft;
	}

	public function getLvl(): ?int
	{
		return $this->lvl;
	}

	public function getParent(): ?self
	{
		return $this->parent;
	}

	public function getRgt(): ?int
	{
		return $this->rgt;
	}

	public function getRoot(): ?self
	{
		return $this->root;
	}

	public function setParent(?self $parent): void
	{
		$this->parent = $parent;
	}

	public function updateParent(self $parent): void
	{
		$this->parent = $parent;
	}

}
