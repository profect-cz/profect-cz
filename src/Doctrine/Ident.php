<?php

declare(strict_types = 1);

namespace Profect\Doctrine;

use Doctrine\ORM\Mapping as ORM;

trait Ident
{

	/** @ORM\Column(type="string", unique=true) */
	protected string $ident;

	public function getIdent(): string
	{
		return $this->ident;
	}

}
