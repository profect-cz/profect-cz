<?php

declare(strict_types = 1);

namespace Profect\Doctrine;

use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;
use Webmozart\Assert\Assert;

trait Softdeletable
{

	/** @ORM\Column(type="datetime_immutable", nullable=true) */
	protected ?DateTimeImmutable $deletedAt = null;

	public function getDeletedAt(): ?DateTimeImmutable
	{
		return $this->deletedAt;
	}

	public function isDeleted(): bool
	{
		return $this->deletedAt !== null;
	}

	public function markAsDeleted(): void
	{
		Assert::null($this->deletedAt, 'Entity already deleted.');

		$this->deletedAt = new DateTimeImmutable();
	}

}
