<?php

declare(strict_types = 1);

namespace Profect\Translator;

use Nette\Localization\ITranslator;

class Translator implements ITranslator
{

	/**
	 * @phpcsSuppress SlevomatCodingStandard.TypeHints.ParameterTypeHint.MissingNativeTypeHint
	 * @phpcsSuppress SlevomatCodingStandard.Functions.UnusedParameter.UnusedParameter
	 * @param string $message
	 * @param string ...$parameters
	 */
	public function translate($message, ...$parameters): string
	{
		return $message;
	}

}
