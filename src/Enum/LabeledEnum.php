<?php

declare(strict_types = 1);

namespace Profect\Enum;

use Webmozart\Assert\Assert;

trait LabeledEnum
{

	abstract public function getLabel(): string;

	/** @return array<array-key, string> */
	final public static function getAvailableLabels(): array
	{
		$labels = [];

		foreach (self::cases() as $enum) {
			$labels[$enum->value] = $enum->getLabel();
		}

		return $labels;
	}

	/** @phpcsSuppress SlevomatCodingStandard.TypeHints.ParameterTypeHint.MissingAnyTypeHint */
	public static function getLabelByValue($value): string
	{
		$labels = self::getAvailableLabels();
		Assert::keyExists($labels, $value);

		return $labels[$value];
	}

}
