<?php

declare(strict_types = 1);

namespace Profect\Enum;

use Consistence\Enum\InvalidEnumValueException;
use Exception;
use Nette\NotImplementedException;

abstract class LabeledMultiEnum extends MultiEnum
{

	/** @return array<string> format: value(integer) => label(string) */
	public function getLabels(): array
	{
		return self::getLabelsByEnum($this);
	}

	/** @return array<string> key(integer) => label(string) */
	public static function getLabelDefinitions(): array
	{
		$singleEnumClass = static::getSingleEnumClass();
		if ($singleEnumClass !== null) {
			$labelDefinitions = [];
			foreach ($singleEnumClass::getAvailableLabels() as $singleEnumValue => $label) {
				$labelDefinitions[static::convertSingleEnumValueToValue($singleEnumValue)] = $label;
			}

			return $labelDefinitions;
		}

		throw new NotImplementedException('This method is "abstract" and should be implemented.');
	}

	/**
	 * @param self<int|string> $labeledMultiEnum
	 * @return array<string> format: value(integer) => label(string)
	 */
	public static function getLabelsByEnum(self $labeledMultiEnum): array
	{
		$labels = [];
		foreach ($labeledMultiEnum->getValues() as $value) {
			$labels[$value] = self::getLabelByValue($value);
		}

		return $labels;
	}

	public static function getLabelByValue(int $value): string
	{
		if (!self::isValidValue($value)) {
			throw new InvalidEnumValueException($value, self::class);
		}

		if (!isset(static::getLabelDefinitions()[$value])) {
			throw new Exception(static::class);
		}

		return static::getLabelDefinitions()[$value];
	}

	/** @return array<string> format: value(integer) => label(string) */
	public static function getLabelsByValue(int $value): array
	{
		return self::getLabelsByEnum(static::get($value));
	}

	public function getLabel(): string
	{
		return self::getLabelByValue($this->getValue());
	}

}
