<?php

declare(strict_types = 1);

namespace Profect\Enum;

trait Enum
{

	public function getValue(): string|int
	{
		return $this->value;
	}

	public function equals(self $enum): bool
	{
		return $this->value === $enum->value;
	}

	/** @return list<self> */
	public static function getAvailableEnums(): array
	{
		return self::cases();
	}

}
