<?php

declare(strict_types = 1);

namespace Profect\Enum;

use Consistence\Enum\MultiEnum as ConsistenceEnum;
use Consistence\Type\ArrayType\ArrayType;
use function constant;
use function sprintf;

class MultiEnum extends ConsistenceEnum
{

	public function __toString(): string
	{
		return (string) $this->getValue();
	}

	/** @return array<int> */
	public function getValues(): array
	{
		return ArrayType::filterValuesByCallback(self::getAvailableValues(), fn (int $value): bool => ($this->getValue() & $value) === $value);
	}

	/** @return array<int> */
	public static function getAvailableValues(): array
	{
		/** @var array<int> */
		return parent::getAvailableValues();
	}

	/**
	 * @phpcsSuppress SlevomatCodingStandard.TypeHints.ParameterTypeHint.MissingNativeTypeHint
	 * @phpcsSuppress SlevomatCodingStandard.Functions.UnusedParameter.UnusedParameter
	 * @param string $name
	 * @param mixed $arguments
	 */
	public static function __callStatic($name, $arguments): parent
	{
		return parent::get(constant(sprintf('%s::%s', static::class, $name)));
	}

}
