<?php

declare(strict_types = 1);

namespace Profect\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\ArgvInput;
use Symfony\Component\Console\Output\ConsoleOutput;

abstract class CronCommand extends Command
{

	public function runByCron(): void
	{
		$this->execute(new ArgvInput(), new ConsoleOutput());
	}

}
