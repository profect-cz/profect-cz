<?php

declare(strict_types = 1);

namespace Profect\Assets;

use Nette\Caching\Cache;
use Nette\Http\Url;
use Nette\Utils\Strings;
use Profect\Cache\CacheService;
use Webmozart\Assert\Assert;
use function file_get_contents;
use function hash_file;
use function sprintf;

class AssetsService
{

	public function __construct(private CacheService $cacheService, private string $wwwDir)
	{
	}

	public function getHash(string $path): string
	{
		$cacheKey = sprintf('assets|%s', Strings::webalize($path));
		$hash = $this->cacheService->load($cacheKey);
		if ($hash === null) {
			$path = sprintf('%s%s', $this->wwwDir, $path);
			$hash = $this->getHashInternal($path);
			$this->cacheService->save($cacheKey, $hash, [
				Cache::FILES => $path,
			]);
		}

		return $hash;
	}

	private function getHashInternal(string $file): string
	{
		$hash = hash_file('md5', $file);
		Assert::notFalse($hash, sprintf('Hash for file: %s is not available.', $file));

		return $hash;
	}

	public function getStyleFromPath(string $path): string
	{
		$url = new Url($path);
		$url->setQuery('');

		$path = sprintf('%s/%s', $this->wwwDir, $url->getAbsoluteUrl());

		$styles = file_get_contents($path);
		Assert::notFalse($styles, 'Missing file');

		return $styles;
	}

}
