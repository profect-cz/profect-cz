<?PHP
declare(strict_types = 1);

namespace Profect\Email;

require_once __DIR__ . '/../bootstrap.php';

use Tester\Assert;
use Tester\TestCase;

class EmailTest extends TestCase
{

	/**
	 * @return string[]
	 */
	public function dataValidEmail(): array
	{
		return [
			['test@asd.cz'],
			['test@cz.com'],
			['tes241t@cz.com'],
			['tes2.41t@cz.com'],
		];
	}

	/**
	 * @return string[]
	 */
	public function dataNotValidEmail(): array
	{
		return [
			['asd.cz'],
			['asd asd@asdasd.cz'],
			['@asdasd.cz'],
			['asdasd@.cz'],
			['..+ě@.cz'],
			['..as@.cz'],
		];
	}

	/**
	 * @param string $email
	 * @dataProvider dataValidEmail
	 */
	public function testValidEmails(string $email): void
	{
		new Email($email);
	}

	/**
	 * @param string $email
	 * @dataProvider dataNotValidEmail
	 */
	public function testNotValidEmails(string $email): void
	{
		Assert::exception(static function () use ($email): void {
			new Email($email);
		}, NotValidEmailAddressException::class);
	}
}

(new EmailTest())->run();
